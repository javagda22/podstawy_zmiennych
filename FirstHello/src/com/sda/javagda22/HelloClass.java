package com.sda.javagda22;

public class HelloClass {

    // public - oznacza ze instrukcja jest dostepna z zewnątrz
    // static - jest dostępna wszędzie (dla całej maszyny wirtualnej)
    // void - instrukcja nie zwraca (nie produkuje) wartości
    public static void main(String[] args) {
        System.out.println("Hello World!"); // enter na końcu z println

        System.out.print("dfghj "); // w jednej linii
        System.out.print("dfghj"); // z tym powyżej
    }
}
