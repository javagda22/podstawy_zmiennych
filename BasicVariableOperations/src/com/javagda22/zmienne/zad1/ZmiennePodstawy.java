package com.javagda22.zmienne.zad1;

public class ZmiennePodstawy {
    /* <- początek bloku komentarza

    Treść komentarza

    koniec bloku komentarza -> */
    public static void main(String[] args) {
        // deklaracja - mówię - istnieje zmienna typu 'int' o nazwie 'zmienna'
        int zmienna = 0; // int - integer ~ -2 000 000 000 - +2 000 000 000

        // przypisanie wartości '23' do zmiennej
        zmienna = 23;

        // linia niżej wypisze tekst o treści 'zmienna'
        System.out.println("zmienna");

        // wypisze wartość która znajduje się pod etykietą 'zmienna'
        System.out.println(zmienna);

        // zmiana wartości
        zmienna = 1000;

        // wypisanie na ekran
        System.out.println("Wartość zmiennej = " + zmienna);
        // instrukcja wyżej pozwala wypisać wartość zmiennej poprzedzoną
        // tekstem "Wartość zmiennej = " -> "Wartość zmiennej = 1000"

        // koniec działania programu. Java sprząta po nas wszystkie zmienne
    }
}
