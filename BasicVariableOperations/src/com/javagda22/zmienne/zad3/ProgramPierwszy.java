package com.javagda22.zmienne.zad3;

public class ProgramPierwszy {
    //    public static void main(String[] args) {
//        String hello = "Hello world!";
//        String myNameText = "mam na imie";
//        String myName = "Paweł Recław";
//
//        System.out.println(hello);
//        System.out.println(myNameText);
//        System.out.println(myName);
//    }
    public static void main(String[] args) {
        String zmienna = "Hello world!";
        System.out.println(zmienna);

        zmienna = "mam na imie";
        System.out.println(zmienna);

        zmienna = "Paweł Recław";
        System.out.println(zmienna);
    }
}
