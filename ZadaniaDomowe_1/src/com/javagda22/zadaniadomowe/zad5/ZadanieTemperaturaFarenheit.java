package com.javagda22.zadaniadomowe.zad5;

public class ZadanieTemperaturaFarenheit {
    public static void main(String[] args) {
        double temperaturaCelsjusz = 20.0;

        // konwersja celsjusz na farenheit
        double temperaturaFarenheit = temperaturaCelsjusz * 1.8 + 32.0;

        System.out.println(temperaturaCelsjusz + " stopni Celsjusza to "
                + temperaturaFarenheit + " stopni Farenheita");
    }
}
