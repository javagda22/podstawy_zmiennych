package com.javagda22.zadaniadomowe.zad_if;

public class ZadaniePodatek {
    public static void main(String[] args) {
        /*
        Napisać program obliczający należny podatek
        dochodowy od osób ﬁzycznych. W programie zdefiniuj
        wartość dochód (double) na pewną wartość i po obliczeniu
        wypisywać na ekranie należny podatek. Podatek
        obliczany jest wg. następujących reguł:

         (jeden if)
        - do 85.528 podatek wynosi 18%
                podstawy minus 556,02 PLN,
         (jeden if)
          - od 85.528 podatek wynosi 14.839,02 zł + 32%
                nadwyżki ponad 85.528,00
         */
        double dochod = 85528.0;
        double podatekDoZaplaty = 0.0;
        if (dochod <= 85528) {
            podatekDoZaplaty = (dochod * 0.18) - 556.02;
        }
        if (dochod > 85528) {
            podatekDoZaplaty = 14839.02 + ((dochod - 85528.0) * 0.32);
        }

        System.out.println("Podatek do zapłaty wynosi: " + podatekDoZaplaty);

    }
}
