package com.javagda22.zadaniadomowe.zad4;

public class ZadanieLogiczne {
    public static void main(String[] args) {
        boolean jest_cieplo = false;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = false;

        // jeśli nie jest ciepło lub wieje wiatr
        boolean ubieram_sie_cieplo = (!jest_cieplo || wieje_wiatr);
        // jeśli nie swieci slonce ale nie wieje wiatr
        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;
        // jesli wieje wiatr, nie ma slonca i nie jest cieplo
        boolean ubieram_kurtke = wieje_wiatr && !swieci_slonce && !jest_cieplo;

        System.out.println("Ubieram sie cieplo: " + ubieram_sie_cieplo);
        System.out.println("Biore parasol: " + biore_parasol);
        System.out.println("Ubieram kurtke: " + ubieram_kurtke);
    }
}
