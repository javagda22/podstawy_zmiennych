package com.javagda22.zadaniadomowe.zad3;

public class ZadaniePrzestawienie {
    public static void main(String[] args) {
        int a = 1;
        int b = 2;
        int c = 3;
        System.out.println("Wartość przed: a=" +a);
        System.out.println("Wartość przed: b=" +b);
        System.out.println("Wartość przed: c=" +c);

        // przestawić a -> b
        // przestawić b -> c
        // przestawić c -> a

        int tymczasoweC = c;
        c = b;
        b = a;
        a = tymczasoweC;


        System.out.println("Wartość po: a=" +a);
        System.out.println("Wartość po: b=" +b);
        System.out.println("Wartość po: c=" +c);
    }
}
