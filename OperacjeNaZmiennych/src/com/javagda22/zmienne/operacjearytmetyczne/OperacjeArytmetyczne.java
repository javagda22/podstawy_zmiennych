package com.javagda22.zmienne.operacjearytmetyczne;

public class OperacjeArytmetyczne {
    public static void main(String[] args) {
        System.out.println("Operacja (2+3) = " + (2 + 3));
        //                                    ^ konkatenacja
        //                                         ^ dodawanie

        System.out.println("Operacja (\"a\" + 2) = " + ("a" + 2));
        //                            ^^ dodanie znaku " jako część string.
        //                            ^^ pokazujemy kompilatorowi że nie ma traktować znaku " jako kończącego ciąg znaków

        System.out.println("Operacja (5 / 2) = " +(5 / 2));
        System.out.println("Operacja (5.0 / 2) = " +(5.0 / 2));
        System.out.println("Operacja (5 / 2.0) = " +(5 / 2.0));
        System.out.println("Operacja (5 / 2.0) = " +(5 / 2.0));
        System.out.println("Operacja (5.0 / 2.0) = " +(5.0 / 2.0));
        System.out.println("Operacja (100L - 10) = " +(100L - 10));
        System.out.println("Operacja (2f - 3) = " +(2f - 3));
        System.out.println("Operacja (5f / 2) = " +(5f / 2));
        System.out.println("Operacja (5d / 2) = " +(5d / 2));
        System.out.println("Operacja ('A' + 2) = " +('A' + 2));
        System.out.println("Operacja ('a' + 2) = " +('a' + 2));
        System.out.println("Operacja (\"a\" + 2) = " +("a" + 2));
        System.out.println("Operacja (\"a\" + \"b\") = " +("a" + "b"));
        System.out.println("Operacja ('a' + 'b') = " +('a' + 'b'));
        System.out.println("Operacja (\"a\" + 'b') = " +("a" + 'b'));
        System.out.println("Operacja (\"a\" + \"b\" + 3) = " +("a" + "b" + 3));
        System.out.println("Operacja ('b' + 3 + \"a\") = " +('b' + 3 + "a"));
        System.out.println("Operacja (9 % 4) = " +(9 % 4));

    }
}
