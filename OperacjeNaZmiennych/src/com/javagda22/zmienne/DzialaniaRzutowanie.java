package com.javagda22.zmienne;

public class DzialaniaRzutowanie {
    public static void main(String[] args) {
        int a;
        int b;

        a = 9;
        b = 10;

        // konkatenacja oraz operacja odejmowania musi być
        // jasno określona. Kompilator wykonuje operacje od lewej
        // tym samym najpierw doda do ciągu znaków wartość zmiennej a
        // a następnie spróbuje odjąć(!) b - nie można odjąć od ciągu znaków
        System.out.println("Wynik działania operacji (a-b)=" + (a - b));

        // zaokrąglenie do pełnej liczby
        System.out.println(a / b);

        short tysiac = 280;
        byte rzut = (byte) tysiac;
        System.out.println("Po rzutowaniu + " + rzut);

        short drugiTysiac = rzut;

        int aa = 5;
        int bb = 7;

        double dd = 6;

        float cc = aa/bb;
        System.out.println(cc);
    }
}
