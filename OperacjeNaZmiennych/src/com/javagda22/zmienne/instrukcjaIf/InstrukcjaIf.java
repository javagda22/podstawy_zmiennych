package com.javagda22.zmienne.instrukcjaIf;

public class InstrukcjaIf {
    public static void main(String[] args) {
        if (2 > 3) {
            System.out.println(":)");
        }
        // oba ify zadziałają tak samo
        if (2 > 3)
            System.out.println(":)");

        ////////
        if (2 > 3) {
            System.out.println(":)");
            System.out.println(":)");
        }
        if (2 > 3)
            System.out.println(":)");
        System.out.println(":)"); // < - ta instrukcja nie należy do bloku if'a
    }
}


//        {
//            double liczba1 = 24.0;
//            double granica = 25.0;
//
//            if (liczba1 > granica) {
//                System.out.println("Liczba jest wyższa niż granica");
//                System.out.println("Wypisanie");
//                liczba1 = liczba1 - 10;
//            }
//            System.out.println(liczba1);
//
//            System.out.println("Koniec programu!");
//        }
//    }
//}
