package com.javagda22.zmienne.operacjelogiczne;

public class OperacjeLogiczne {
    public static void main(String[] args) {
        System.out.println("Operacja (false == false) = " + (false == false));
        System.out.println("Operacja (false != true) = " + (false != true));
        System.out.println("Operacja (!true) = " + (!true));
        System.out.println("Operacja (2>4) = " + (2 > 4));
        System.out.println("Operacja (3>5) = " + (3 > 5));
        System.out.println("Operacja (3==3 && 3==4) = " + (3 == 3 && 3 == 4));
        System.out.println("Operacja (3!=5 || 3==5) = " + (3 != 5 || 3 == 5));
        System.out.println("Operacja ((2+4) > (1+3)) = " + ((2 + 4) > (1 + 3)));
        System.out.println("Operacja (\"cos\".equals(\"cos\")) = " + ("cos".equals("cos")));
        System.out.println("Operacja (\"cos\" ==\"cos\") = " + ("cos" == "cos"));
    }
}
