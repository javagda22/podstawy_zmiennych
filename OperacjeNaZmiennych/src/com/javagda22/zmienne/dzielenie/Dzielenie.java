package com.javagda22.zmienne.dzielenie;

public class Dzielenie {
    public static void main(String[] args) {
        int a, b, c;

        a = 1;
        b = 2;
        c = 3;

        System.out.println(a / 2);
        System.out.println(a / b);
        System.out.println(1 / 1);
        System.out.println(a / (b / c));

        c = a / b;
        System.out.println(c);
    }
}
