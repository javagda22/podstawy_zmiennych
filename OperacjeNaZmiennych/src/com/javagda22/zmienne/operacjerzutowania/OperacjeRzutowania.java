package com.javagda22.zmienne.operacjerzutowania;

public class OperacjeRzutowania {
    // slajd 56
    public static void main(String[] args) {
        byte zByte = 1;
        char zChar = 'a';
        short zShort = 2;
        int zInt = 19;
        long zLong = 12;
        float zFloat = 15f;
        double zDouble = 16d;

        // (65535) -> (2xxxxxxxxx)
        // short -> int
        int wynikShortInt = zShort;
        // (65535) -> (20 cyfr)
        // short -> long
        long wynikShortLong = zShort;
        // (2xxxxxxxxx) -> (14 cyfr, ...)
        // int -> double
        double wynikIntDouble = zInt;
        // (20 cyfr) -> ( 10 cyfr)
        // long -> int
        int wynikLongInt = (int) zLong;
        // (65535) -> ( 255)
        // short -> byte
        byte wynikShortByte = (byte) zShort;
        // char -> int
        int wynikCharInt = zChar;


        int doubleNaInt = (int) zDouble;
    }
}
